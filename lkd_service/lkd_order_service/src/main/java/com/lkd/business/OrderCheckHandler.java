package com.lkd.business;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.lkd.annotations.ProcessType;
import com.lkd.common.VMSystem;
import com.lkd.contract.server.OrderCheck;
import com.lkd.entity.OrderEntity;
import com.lkd.service.OrderService;
import com.lkd.utils.JsonUtil;
import jdk.internal.joptsimple.internal.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;
//订单状态后续的检查（创建订单的时候去发送）
@Component
@ProcessType(value = "orderCheck")
public class OrderCheckHandler implements MsgHandler{
    @Autowired
    private OrderService orderService;
    @Override
    public void process(String jsonMsg) throws IOException {
        OrderCheck orderCheck = JsonUtil.getByJson(jsonMsg, OrderCheck.class);
        if (orderCheck==null|| Strings.isNullOrEmpty(orderCheck.getOrderNo()))return;
        //修改订单状态
        QueryWrapper<OrderEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(OrderEntity::getOrderNo, orderCheck.getOrderNo())
                .eq(OrderEntity::getStatus, VMSystem.ORDER_STATUS_CREATE);
        OrderEntity one = orderService.getOne(qw);
        if (one==null|| !Objects.equals(one.getStatus(), VMSystem.ORDER_STATUS_CREATE))return;
       //其他的就修改订单状态为无效
        UpdateWrapper<OrderEntity> uw = new UpdateWrapper<>();
        uw.lambda()
                .eq(OrderEntity::getOrderNo, orderCheck.getOrderNo())
                .set(OrderEntity::getStatus, VMSystem.ORDER_STATUS_INVALID);
        orderService.update(uw);
    }
}


















