package com.lkd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Strings;
import com.lkd.common.VMSystem;
import com.lkd.entity.OrderCollectEntity;
import com.lkd.service.OrderCollectService;
import com.lkd.service.ReportService;
import com.lkd.viewmodel.BarCharCollect;
import com.lkd.viewmodel.Pager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.metrics.ParsedSum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportServiceImpl implements ReportService {
    private final OrderCollectService orderCollectService;
    private final RestHighLevelClient restHighLevelClient;

    /**
     * 获取合作商分账汇总信息 分账查询
     *
     * @param pageIndex
     * @param pageSize
     * @param OwnerName
     * @param start
     * @param end
     * @return
     */
    @Override
    public Pager<OrderCollectEntity> getPartnerCollect(Long pageIndex, Long pageSize, String name, LocalDate start, LocalDate end) {
        Page<OrderCollectEntity> page = new Page<>(pageIndex, pageSize);
        QueryWrapper<OrderCollectEntity> qw = new QueryWrapper<>();
        qw.select("IFNULL(sum(order_count),0) as order_count",
                "IFNULL(sum(total_bill),0) as total_bill",
                "IFNULL(sum(order_total_money),0) as order_total_money",
                "IFNULL(min(ratio)),0 as ratio"
        );
        if (!Strings.isNullOrEmpty(name)) {
            qw.lambda().like(OrderCollectEntity::getOwnerName, name);
        }
        qw.lambda().
                ge(OrderCollectEntity::getDate, start)
                .le(OrderCollectEntity::getDate, end)
                .groupBy(OrderCollectEntity::getOwnerId, OrderCollectEntity::getDate)
                .orderByDesc(OrderCollectEntity::getDate);

        return Pager.build(orderCollectService.page(page, qw));
    }

    /**
     * 获取合作商前12条点位分账数据
     *
     * @param partnerId
     * @return
     */
    @Override
    public List<OrderCollectEntity> getTop12(Integer partnerId) {

        LambdaQueryWrapper<OrderCollectEntity> qw = new LambdaQueryWrapper<>();
        qw.select(OrderCollectEntity::getOwnerName,
                OrderCollectEntity::getTotalBill,
                OrderCollectEntity::getOrderCount,
                OrderCollectEntity::getDate
        );
        qw.eq(OrderCollectEntity::getOwnerId, partnerId);
        qw.orderByDesc(OrderCollectEntity::getDate);
        qw.last("limit 12");

        return orderCollectService.list(qw);
    }

    /**
     * 合作商点位分账搜索
     *
     * @param partnerId
     * @param nodeName
     * @param start
     * @param end
     * @return
     */


//      * @param pageIndex
//     * @param pageSize
//     * @param OwnerName
//     * @param start
//     * @param end
    @Override
    public Pager<OrderCollectEntity> search(Long pageIndex, Long pageSize, Integer partnerId, String nodeName, LocalDate start, LocalDate end) {
        LambdaQueryWrapper<OrderCollectEntity> qw = new LambdaQueryWrapper<>();
        qw.select(OrderCollectEntity::getOwnerName,
                OrderCollectEntity::getTotalBill,
                OrderCollectEntity::getOrderCount,
                OrderCollectEntity::getDate
        ).eq(OrderCollectEntity::getOwnerId, partnerId);
        if (Strings.isNullOrEmpty(nodeName)) {
            qw.like(OrderCollectEntity::getNodeName, nodeName);
        }
        if (start != null && end != null) {
            qw.le(OrderCollectEntity::getDate, end);
            qw.ge(OrderCollectEntity::getDate, start);
        }
        qw.orderByDesc(OrderCollectEntity::getDate);
        Page<OrderCollectEntity> collectEntityPage = new Page<>(pageIndex, pageSize);
        return Pager.build(orderCollectService.page(collectEntityPage, qw));
    }

    /**
     * 导出数据
     *
     * @param partnerId
     * @param nodeName
     * @param start
     * @param end
     * @return
     */

    @Override
    public List<OrderCollectEntity> getList(Integer partnerId, String nodeName, LocalDate start, LocalDate end) {
        LambdaQueryWrapper<OrderCollectEntity> qw = new LambdaQueryWrapper<>();
        qw.select(OrderCollectEntity::getOwnerName,
                OrderCollectEntity::getTotalBill,
                OrderCollectEntity::getOrderCount,
                OrderCollectEntity::getDate
        ).eq(OrderCollectEntity::getOwnerId, partnerId);
        if (Strings.isNullOrEmpty(nodeName)) {
            qw.like(OrderCollectEntity::getNodeName, nodeName);
        }
        if (start != null && end != null) {
            qw.le(OrderCollectEntity::getDate, end);
            qw.ge(OrderCollectEntity::getDate, start);
        }
        return orderCollectService.list(qw);
    }

    //数据展示
    @Override
    public BarCharCollect getCollect(Integer partnerId, LocalDate start, LocalDate end) {
        var queryWrapper = new QueryWrapper<OrderCollectEntity>();
        queryWrapper.select("IFNULL(sum(total_bill),0) as total_bill", "date")
                .lambda().ge(OrderCollectEntity::getDate, start)
                .le(OrderCollectEntity::getDate, end)
                .eq(OrderCollectEntity::getOwnerId, partnerId);
        Map<LocalDate, Integer> mapCollect = orderCollectService.list(queryWrapper).stream().
                collect(Collectors.toMap(OrderCollectEntity::getDate, OrderCollectEntity::getTotalBill));
        BarCharCollect result = new BarCharCollect();
        start.datesUntil(end.plusDays(1), Period.ofDays(1)).forEach(date -> {
            result.getXAxis().add(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
            if (mapCollect.containsKey(date)) {
                result.getSeries().add(mapCollect.get(date));
            }
            result.getSeries().add(0);
        });
        return result;
    }

    @Override
    public BarCharCollect getAmountCollect(Integer collectType, LocalDate start, LocalDate end) {
//月分组式聚合
        //展示日期
        var showdate = collectType == 2 ? "min(date) as date" : "date";
        //分组日期
        var groupdate = collectType == 2 ? "MONTH(date)" : "date";
        //格式化日期
        var formatter = collectType == 2 ? DateTimeFormatter.ofPattern("yyyy-MM") : DateTimeFormatter.ISO_LOCAL_DATE;
        //间隔天数
        var gapdate = collectType == 2 ? Period.ofMonths(1) : Period.ofDays(1);
        var qw = new QueryWrapper<OrderCollectEntity>();
        qw.select("IFNULL(sum(order_total_money),0) as order_total_money", showdate)
                .groupBy(groupdate)
                .lambda().ge(OrderCollectEntity::getDate, start)
                .le(OrderCollectEntity::getDate, end)
                .orderByAsc(OrderCollectEntity::getDate);
        Map<String, Integer> collectMap = orderCollectService.list(qw).stream().collect(Collectors.toMap(o ->
                o.getDate().format(formatter), OrderCollectEntity::getOrderTotalMoney)
        );
//创建的日期map 必须和创建的日期间隔数保持一致
        var result = new BarCharCollect();
        start.datesUntil(end.plusDays(1), gapdate).forEach(
                date -> {
                    String key = date.format(formatter);
                    result.getXAxis().add(key);
                    if (collectMap.containsKey(key)) {
                        result.getSeries().add(collectMap.get(key));
                    } else {
                        result.getSeries().add(0);
                    }
                }
        );
        return result;
    }

    @Override
    public BarCharCollect getCollectByRegion(LocalDate start, LocalDate end) {
        //es查询基本条件
        SearchRequest searchRequest = new SearchRequest("order");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //根据时间范围搜索
        boolQueryBuilder.filter(QueryBuilders.rangeQuery("create_time").gte(start).lte(end));
        boolQueryBuilder.filter(QueryBuilders.termQuery("pay_status", VMSystem.PAY_STATUS_PAYED));
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        //根据区域分组
        AggregationBuilder regionAgg = AggregationBuilders
                .terms("region")
                .field("region_name")
                .subAggregation(AggregationBuilders.sum("amount_sum").field("amount"))
                .order(BucketOrder.aggregation("amount_sum", false))
                .size(30);
        sourceBuilder.aggregation(regionAgg);
        searchRequest.source(sourceBuilder);
        var result = new BarCharCollect();
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            var aggregations = searchResponse.getAggregations();
            if (aggregations == null) return result;
            var term = (ParsedStringTerms) aggregations.get("region");
            var buckets = term.getBuckets();
            if (buckets.size() == 0) return result;
            buckets.stream().forEach(b -> {
                result.getXAxis().add(b.getKeyAsString());
                var amountSum = (ParsedSum) b.getAggregations().get("amount_sum");
                Double value = amountSum.getValue();
                result.getSeries().add(value.intValue());
            });
        } catch (IOException e) {
            log.error("根据区域汇总数据出错", e);
        }
        return result;
    }
}




















