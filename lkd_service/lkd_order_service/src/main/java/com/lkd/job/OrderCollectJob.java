package com.lkd.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lkd.common.VMSystem;
import com.lkd.entity.OrderCollectEntity;
import com.lkd.entity.OrderEntity;
import com.lkd.feignService.UserService;
import com.lkd.feignService.VMService;
import com.lkd.service.OrderCollectService;
import com.lkd.service.OrderService;
import com.lkd.viewmodel.PartnerViewModel;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Component
@Slf4j
public class OrderCollectJob {
    private final OrderService orderService;
    private final OrderCollectService orderCollectService;
    private final UserService userService;
    private final VMService vmService;


    /**
     * 订单合并
     *当天的聚合
     * @param param
     * @return
     */
    @XxlJob("orderCollectJobHandler")
    public ReturnT<String> collectTask(String param) {
        LocalDate yesterday = LocalDate.now().plusDays(-1);
        QueryWrapper<OrderEntity> qw = new QueryWrapper<>();
        qw.select("owner_id", "IFNULL(sum(amount),0) as amount", "IFNULL(sum(bill),0) as bill", "IFNULL(count(1),0) as price")
                .lambda().ge(OrderEntity::getCreateTime, yesterday).le(OrderEntity::getCreateTime, LocalDate.now())
                .eq(OrderEntity::getPayStatus, VMSystem.PAY_STATUS_PAYED)
                .groupBy(OrderEntity::getOwnerId, OrderEntity::getNodeId);
        List<OrderEntity> OrderEntityList = orderService.list(qw);
        if (OrderEntityList == null) {
            log.error("未查询到该点位合作社的分账信息");
        }
        assert OrderEntityList != null;
        OrderEntityList.forEach(orderEntity -> {
            OrderCollectEntity orderCollectEntity = new OrderCollectEntity();
            orderCollectEntity.setOwnerId(orderEntity.getOwnerId());
            PartnerViewModel partner = userService.getPartner(orderEntity.getOwnerId());
            orderCollectEntity.setOwnerName(partner.getName());
            orderCollectEntity.setNodeId(orderEntity.getNodeId());
            orderCollectEntity.setNodeName(vmService.getNodeName(orderEntity.getNodeId()));
            orderCollectEntity.setTotalBill(orderEntity.getBill());//分成
            orderCollectEntity.setOrderCount(orderEntity.getPrice());//订单数
            orderCollectEntity.setOrderTotalMoney(orderEntity.getAmount());//总钱数
            orderCollectEntity.setDate(yesterday);
            orderCollectEntity.setRatio(partner.getRatio());
            orderCollectService.save(orderCollectEntity);
        });
        return ReturnT.SUCCESS;
    }

}



























