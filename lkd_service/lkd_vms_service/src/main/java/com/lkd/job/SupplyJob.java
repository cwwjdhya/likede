package com.lkd.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lkd.common.VMSystem;
import com.lkd.entity.VendingMachineEntity;
import com.lkd.feignService.TaskService;
import com.lkd.service.VendingMachineService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.util.ShardingUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 自动补货任务生成Job
 */
@Component
@Slf4j
public class SupplyJob {

    @Autowired
    private VendingMachineService vmService;
    @Autowired
    private TaskService taskService;

    /**
     * 售货机扫描任务
     */
    @XxlJob("supplyJobHandler")
    public ReturnT<String> supplyJobHandler(String param) throws Exception {
        ShardingUtil.ShardingVO shardingVo = ShardingUtil.getShardingVo();
        int total = shardingVo.getTotal();//总片数
        int index = shardingVo.getIndex();//当前索引
        Integer percent = taskService.getSupplyAlertValue();//获取警戒线百分比
        QueryWrapper<VendingMachineEntity> qw = new QueryWrapper<>();
        qw
                .lambda()
                .eq(VendingMachineEntity::getVmStatus, VMSystem.VM_STATUS_RUNNING)
                .apply("mod(id," + total + ")=" + index);

        List<VendingMachineEntity> list = vmService.list(qw);
        for (VendingMachineEntity vm : list) {
            XxlJobLogger.log("扫描售货机" + vm.getInnerCode());
            int inventory = vmService.inventory(percent, vm);
            if (inventory > 0) {
                vmService.sendSupplyTask(vm);
                XxlJobLogger.log("售货机" + vm.getInnerCode() + "缺货");

            }
        }
        return ReturnT.SUCCESS;
    }
}
