//package com.lkd.file;
//
//import com.alibaba.excel.context.AnalysisContext;
//import com.alibaba.excel.event.AnalysisEventListener;
//import com.google.common.collect.Lists;
//import com.lkd.entity.SkuEntity;
//import com.lkd.service.SkuService;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ExcelDataListener <E> extends AnalysisEventListener<E> {
//    @Autowired
//    private SkuService skuService;
//    private List<SkuEntity> list= Lists.newArrayList();
//
//    private static final int BATCH_COUNT=500;//每批存储条数
//    /**
//     * 提取数据 一条记录
//     * @param e 每条import记录
//     * @param analysisContext
//     */
//    @Override
//    public void invoke(E e, AnalysisContext analysisContext) {
//        SkuEntity skuEntity = new SkuEntity();
//        BeanUtils.copyProperties(e,skuEntity);
//        list.add(skuEntity);
//        if(list.size()>=BATCH_COUNT){
//            doAfterAllAnalysed(null);
//        }
//    }
//
//    /**
//     * 入库
//     * @param analysisContext
//     */
//    @Override
//    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        skuService.saveBatch(list);
//        list.clear();
//    }
//}
