package com.lkd.job;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.lkd.common.VMSystem;
import com.lkd.entity.TaskCollectEntity;
import com.lkd.entity.TaskEntity;
import com.lkd.service.TaskCollectService;
import com.lkd.service.TaskService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.var;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public class TaskCollectJob {
    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskCollectService taskCollectService;

    /**
     * 每日工单数据汇总(汇总昨天数据)
     *
     * @return
     */
    @XxlJob("taskCollectJobHandler")
    public ReturnT<String> collectTask(String param) {
        //把每天的数据做汇总插入
        TaskCollectEntity taskCollectEntity = new TaskCollectEntity();
        LocalDate start = LocalDate.now().plusDays(-1);
        taskCollectEntity.setCollectDate(start);//收集日期
        taskCollectEntity.setCancelCount(this.count(start, VMSystem.TASK_STATUS_CANCEL));//取消数
        taskCollectEntity.setFinishCount(this.count(start, VMSystem.TASK_STATUS_FINISH));//完成数
        taskCollectEntity.setProgressCount(this.count(start, VMSystem.TASK_STATUS_PROGRESS));//正在处理
        cleanTask();
        clearData(start);
        taskCollectService.save(taskCollectEntity);

        return ReturnT.SUCCESS;
    }

    //   将工单表中日期小于当天，并且状态为创建或进行中的工单（非完成和取消）修改为取消状态。
    private void cleanTask() {
       var qw = new LambdaUpdateWrapper<TaskEntity>();
        qw.lt(TaskEntity::getUpdateTime,LocalDate.now())
                .and(w->w.eq(TaskEntity::getTaskStatus,VMSystem.TASK_STATUS_PROGRESS).or()
                        .eq(TaskEntity::getTaskStatus,VMSystem.TASK_STATUS_CREATE))
                .set(TaskEntity::getTaskStatus,VMSystem.TASK_STATUS_CANCEL)
                .set(TaskEntity::getDesc,"订单超时");
        taskService.update(qw);
    }

    private int count(LocalDate start, Integer status) {
        LambdaQueryWrapper<TaskEntity> qw = new LambdaQueryWrapper<>();
        qw.ge(TaskEntity::getUpdateTime, start)
                .eq(TaskEntity::getTaskStatus, status)
                .lt(TaskEntity::getUpdateTime, start.plusDays(1));
        return taskService.count(qw);
    }

    private void clearData(LocalDate start) {
        var qw = new LambdaQueryWrapper<TaskCollectEntity>();
        qw.eq(TaskCollectEntity::getCollectDate, start);
        taskCollectService.remove(qw);
    }


}
