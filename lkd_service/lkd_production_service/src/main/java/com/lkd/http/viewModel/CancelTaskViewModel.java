package com.lkd.http.viewModel;

import lombok.Data;

import java.io.Serializable;

@Data
public class CancelTaskViewModel implements Serializable {
    /**
     * 拒绝理由
     */
    private String desc;
}