package com.lkd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.lkd.common.VMSystem;
import com.lkd.config.TopicConfig;
import com.lkd.contract.SupplyCfg;
import com.lkd.contract.SupplyChannel;
import com.lkd.contract.TaskCompleteContract;
import com.lkd.dao.TaskDao;
import com.lkd.emq.MqttProducer;
import com.lkd.entity.TaskCollectEntity;
import com.lkd.entity.TaskDetailsEntity;
import com.lkd.entity.TaskEntity;
import com.lkd.entity.TaskStatusTypeEntity;
import com.lkd.exception.LogicException;
import com.lkd.feignService.UserService;
import com.lkd.feignService.VMService;
import com.lkd.http.viewModel.CancelTaskViewModel;
import com.lkd.http.viewModel.TaskReportInfo;
import com.lkd.http.viewModel.TaskViewModel;
import com.lkd.service.TaskCollectService;
import com.lkd.service.TaskDetailsService;
import com.lkd.service.TaskService;
import com.lkd.service.TaskStatusTypeService;
import com.lkd.viewmodel.Pager;
import com.lkd.viewmodel.SingletonUserWorks;
import com.lkd.viewmodel.UserWork;
import com.lkd.viewmodel.VendingMachineViewModel;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TaskServiceImpl extends ServiceImpl<TaskDao, TaskEntity> implements TaskService  {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private TaskDetailsService taskDetailsService;

    @Autowired
    private VMService vmService;

    @Autowired
    private TaskStatusTypeService statusTypeService;

    @Autowired
    private UserService userService;
    @Autowired
    private MqttProducer mqttProducer;

    @Override
    public boolean acceptTask(Long id) {
        TaskEntity task = this.getById(id);
        if (task.getTaskStatus() != VMSystem.TASK_STATUS_CREATE) {
            throw new LogicException("工单状态不是待处理");
        }
        task.setTaskStatus(VMSystem.TASK_STATUS_PROGRESS);
        return this.updateById(task);
    }

    @Override
    public boolean cancelTask(Long id, CancelTaskViewModel cancelVo) {
        TaskEntity task = this.getById(id);
        //创建可以取消，接受以后也可以取消 取消描述
        if (task.getTaskStatus() == VMSystem.TASK_STATUS_CANCEL || task.getTaskStatus() == VMSystem.TASK_STATUS_FINISH) {
            throw new LogicException("工单状态已经结束");
        }
        task.setTaskStatus(VMSystem.TASK_STATUS_CANCEL);
        task.setDesc(cancelVo.getDesc());
        updateTaskZSet(task, -1);
        return this.updateById(task);
    }


    /**
     * 代码优化
     *
     * @param taskViewModel
     * @return
     * @throws LogicException
     */
    @Override
    @Transactional(rollbackFor = {Exception.class}, noRollbackFor = {LogicException.class})
    public boolean createTask(TaskViewModel taskViewModel) throws LogicException {
        checkCreateTask(taskViewModel.getInnerCode(), taskViewModel.getProductType());//验证
        if (hasTask(taskViewModel.getInnerCode(), taskViewModel.getProductType())) {
            throw new LogicException("该机器有未完成的同类型工单");
        }

        //向工单表插入一条记录
        TaskEntity taskEntity = new TaskEntity();
        BeanUtils.copyProperties(taskViewModel, taskEntity);
        //工单号
        taskEntity.setTaskCode(this.generateTaskCode());
        taskEntity.setTaskStatus(VMSystem.TASK_STATUS_CREATE);
        taskEntity.setProductTypeId(taskViewModel.getProductType());
        taskEntity.setAddr(vmService.getVMInfo(taskViewModel.getInnerCode()).getNodeAddr());
        taskEntity.setRegionId(vmService.getVMInfo(taskViewModel.getInnerCode()).getRegionId());
        this.save(taskEntity);
        //如果是补货工单，向 工单明细表插入记录
        if (taskEntity.getProductTypeId() == VMSystem.TASK_TYPE_SUPPLY) {
            taskViewModel.getDetails().forEach(d -> {
                TaskDetailsEntity detailsEntity = new TaskDetailsEntity();
                BeanUtils.copyProperties(d, detailsEntity);
                detailsEntity.setTaskId(taskEntity.getTaskId());
                taskDetailsService.save(detailsEntity);
            });
        }
        updateTaskZSet(taskEntity, 1);
        return true;
    }


    @Override
    public boolean completeTask(long id) {
        return this.completeTask(id, 0d, 0d, "");
    }

    //完成工单分为两类（运维、运营工单,补货工单 ）
    @Override
    @Transactional
    public boolean completeTask(long id, Double lon, Double lat, String addr) {
        TaskEntity taskEntity = this.getById(id);
        if (taskEntity.getTaskStatus() == VMSystem.TASK_STATUS_CANCEL || taskEntity.getTaskStatus() == VMSystem.TASK_STATUS_FINISH) {
            throw new LogicException("该工单已结束");
        }
        taskEntity.setTaskStatus(VMSystem.TASK_STATUS_FINISH);
        //地址存入数据库当中
        taskEntity.setAddr(addr);
        this.updateById(taskEntity);
        // 向消息队列发送消息，通知售货机更改状态
        //如果是投放工单或撤机工单
        if (taskEntity.getProductTypeId() == VMSystem.TASK_TYPE_DEPLOY || taskEntity.getProductTypeId() == VMSystem.TASK_TYPE_REVOKE) {
            noticeVMServiceStatus(lon, lat, taskEntity);
        }
        //如果是补货工单
        if (taskEntity.getProductTypeId() == VMSystem.TASK_TYPE_SUPPLY) {
            noticeVMServiceSupply(taskEntity);
        }
        return true;
    }


    private void noticeVMServiceStatus(Double lon, Double lat, TaskEntity taskEntity) {
        TaskCompleteContract taskCompleteContract = new TaskCompleteContract();
        taskCompleteContract.setTaskType(taskEntity.getProductTypeId());
        taskCompleteContract.setInnerCode(taskEntity.getInnerCode());
        taskCompleteContract.setLon(lon);
        taskCompleteContract.setLat(lat);

        try {
            //发送消息让msgHandler去获得的具体的Handler解决问题
            mqttProducer.send(TopicConfig.COMPLETED_TASK_TOPIC, 2, taskCompleteContract);
        } catch (JsonProcessingException e) {
            log.error("发送mq投放或撤机协议消息失败");
            throw new LogicException("发送mq投放或撤机协议消息失败");
        }
    }


    @Override
    public List<TaskStatusTypeEntity> getAllStatus() {
        QueryWrapper<TaskStatusTypeEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .ge(TaskStatusTypeEntity::getStatusId, VMSystem.TASK_STATUS_CREATE);

        return statusTypeService.list(qw);
    }

    @Override
    public Pager<TaskEntity> search(Long pageIndex, Long pageSize, String innerCode, Integer userId, String taskCode, Integer status, Boolean isRepair, String start, String end) {
        Page<TaskEntity> page = new Page<>(pageIndex, pageSize);
        LambdaQueryWrapper<TaskEntity> qw = new LambdaQueryWrapper<>();
        if (!Strings.isNullOrEmpty(innerCode)) {
            qw.eq(TaskEntity::getInnerCode, innerCode);
        }
        if (userId != null && userId > 0) {
            qw.eq(TaskEntity::getAssignorId, userId);
        }
        if (!Strings.isNullOrEmpty(taskCode)) {
            qw.like(TaskEntity::getTaskCode, taskCode);
        }
        if (status != null && status > 0) {
            qw.eq(TaskEntity::getTaskStatus, status);
        }
        if (isRepair != null) {
            if (isRepair) {
                qw.ne(TaskEntity::getProductTypeId, VMSystem.TASK_TYPE_SUPPLY);
            } else {
                qw.eq(TaskEntity::getProductTypeId, VMSystem.TASK_TYPE_SUPPLY);
            }
        }
        if (!Strings.isNullOrEmpty(start) && !Strings.isNullOrEmpty(end)) {
            qw
                    .ge(TaskEntity::getCreateTime, LocalDate.parse(start, DateTimeFormatter.ISO_LOCAL_DATE))
                    .le(TaskEntity::getCreateTime, LocalDate.parse(end, DateTimeFormatter.ISO_LOCAL_DATE));
        }
        //根据最后更新时间倒序排序
        qw.orderByDesc(TaskEntity::getUpdateTime);

        return Pager.build(this.page(page, qw));
    }


    /**
     * 补货协议封装与下发
     *
     * @param taskEntity
     */
    private void noticeVMServiceSupply(TaskEntity taskEntity) {

        QueryWrapper<TaskDetailsEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(TaskDetailsEntity::getTaskId, taskEntity.getTaskId());
        List<TaskDetailsEntity> details = taskDetailsService.list(qw);
        SupplyCfg supplyCfg = new SupplyCfg();
        supplyCfg.setInnerCode(taskEntity.getInnerCode());
        List<SupplyChannel> supplyChannels = Lists.newArrayList();
        details.forEach(d -> {
            SupplyChannel channel = new SupplyChannel();
            channel.setChannelId(d.getChannelCode());
            channel.setCapacity(d.getExpectCapacity());
            supplyChannels.add(channel);
        });
        supplyCfg.setSupplyData(supplyChannels);

        try {
            //发送消息让msgHandler去获得的具体的Handler解决问题
            mqttProducer.send(TopicConfig.COMPLETED_TASK_TOPIC, 2, supplyCfg);
        } catch (JsonProcessingException e) {
            log.error("发送mq补货协议消息失败");
            throw new LogicException("发送mq补货协议消息失败");
        }

    }

    /**
     * 同一台设备下是否存在未完成的工单
     *
     * @param innerCode
     * @param productionType
     * @return
     */
    private boolean hasTask(String innerCode, int productionType) {
        QueryWrapper<TaskEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .select(TaskEntity::getTaskId)
                .eq(TaskEntity::getInnerCode, innerCode)
                .eq(TaskEntity::getProductTypeId, productionType)
                .le(TaskEntity::getTaskStatus, VMSystem.TASK_STATUS_PROGRESS);

        return this.count(qw) > 0;
    }

    /**
     * @param innerCode   设备编号
     * @param productType 设备状态
     * @throws LogicException
     */

    private void checkCreateTask(String innerCode, int productType) throws LogicException {
        VendingMachineViewModel vmInfo = vmService.getVMInfo(innerCode);//根据设备编号查询设备
        if (vmInfo == null) throw new LogicException("设备校验失败");
        if (productType == VMSystem.TASK_TYPE_DEPLOY && vmInfo.getVmStatus() == VMSystem.VM_STATUS_RUNNING) {
            throw new LogicException("该设备已在运营");
        }

        if (productType == VMSystem.TASK_TYPE_SUPPLY && vmInfo.getVmStatus() != VMSystem.VM_STATUS_RUNNING) {
            throw new LogicException("该设备不在运营状态");
        }

        if (productType == VMSystem.TASK_TYPE_REVOKE && vmInfo.getVmStatus() != VMSystem.VM_STATUS_RUNNING) {
            throw new LogicException("该设备不在运营状态");
        }
    }


    /**
     * 生成工单编码
     *
     * @return
     */
    private String generateTaskCode() {
        String key = "lkd.task.code." + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj == null) {
            redisTemplate.opsForValue().set(key, 1L, Duration.ofDays(1));
            return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + Strings.padStart("1", 4, '0');
        }

        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + Strings.padStart(redisTemplate.opsForValue().increment(key, 1).toString(), 4, '0');
    }
    //刷新个人员工工单列表

    /**
     * 更新工单量列表
     *
     * @param taskEntity
     * @param score
     */
    private void updateTaskZSet(TaskEntity taskEntity, int score) {
        String roleCode = "1003";//运维
        if (taskEntity.getProductTypeId().intValue() == 2) {//补货工单运营
            roleCode = "1002";
        }
        String key = VMSystem.REGION_TASK_KEY_PREF +
                LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))
                + "." + taskEntity.getRegionId()
                + "." + roleCode;
        redisTemplate.opsForZSet().incrementScore(key, taskEntity.getAssignorId(), score);
    }

    //获得最少工单的人
    @Override
    public Integer getLeastUser(Integer regionId, Boolean isRepair) {
        String roleCode = "1002";
        if (isRepair) {
            roleCode = "1003";
        }
        String key = VMSystem.REGION_TASK_KEY_PREF +
                LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))
                + "." + regionId
                + "." + roleCode;
        Set<Object> userId = redisTemplate.opsForZSet().range(key, 0, 1);//开始排名为0，结束排名为1，因此返回的结果就是排名为0和1的两个元素
        if (userId.size() <= 0 || regionId == null) {
            throw new LogicException("该区域并没有相关的运维或运营人员");
        }
        return (Integer) userId.stream().collect(Collectors.toList()).get(0);
    }

    @Override
    public List<TaskReportInfo> getTaskReportInfo(LocalDateTime start, LocalDateTime end) {
        //运营工单总数total
        var supplyTotalFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, false, null));
        //运维工单总数total
        var repairTotalFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, true, null));
        //运营完成工单总数total
        var completedSupplyFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, false, VMSystem.TASK_STATUS_FINISH));
        //运维完成工单总数total
        var completedRepairFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, true, VMSystem.TASK_STATUS_FINISH));
        //运营取消工单总数total
        var cancelSupplyFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, false, VMSystem.TASK_STATUS_CANCEL));
        //运维取消工单总数total
        var cancelRepairFuture = CompletableFuture.supplyAsync(() -> taskCount(start, end, true, VMSystem.TASK_STATUS_CANCEL));

        //运营人员总数
        var operatorCountFuture = CompletableFuture.supplyAsync(() -> userService.getOperatorCount());
        //运维人员总数
        var repairerCountFuture = CompletableFuture.supplyAsync(() -> userService.getRepairerCount());
        CompletableFuture.allOf(
                supplyTotalFuture,
                repairTotalFuture,
                completedSupplyFuture,
                completedRepairFuture,
                cancelSupplyFuture,
                cancelRepairFuture,
                operatorCountFuture,
                repairerCountFuture
        ).join();
        List<TaskReportInfo> result = Lists.newArrayList();
        var supplyTaskInfo = new TaskReportInfo();
        var repairTaskInfo = new TaskReportInfo();
        try {
            supplyTaskInfo.setTotal(supplyTotalFuture.get());
            supplyTaskInfo.setCompletedTotal(completedSupplyFuture.get());
            supplyTaskInfo.setRepair(false);
            supplyTaskInfo.setCancelTotal(cancelSupplyFuture.get());
            supplyTaskInfo.setDate(SimpleDateFormat.getDateInstance().format(new Date()));

            repairTaskInfo.setTotal(repairTotalFuture.get());
            repairTaskInfo.setCompletedTotal(completedRepairFuture.get());
            repairTaskInfo.setRepair(true);
            repairTaskInfo.setCancelTotal(cancelRepairFuture.get());
            repairTaskInfo.setDate(SimpleDateFormat.getDateInstance().format(new Date()));

            result.add(supplyTaskInfo);
            result.add(repairTaskInfo);
        } catch (Exception e) {
            log.error("构建工单统计数据失败", e);
        }
        return result;
    }

    //根据工单状态，获取用户(当月)工单数
    @Override
    public UserWork getUserWork(Integer userId, LocalDateTime start, LocalDateTime end) {
        UserWork userWork = new UserWork();
        //总工单
        var totalFuture = CompletableFuture.supplyAsync(() -> getCountByUserId(userId, start, end, null))
                .whenComplete((r, e) -> {
                    if (e != null) {
                        userWork.setTotal(0);
                        log.error("user work error",e);
                    } else {
                        userWork.setTotal(r);
                    }
                });
        //完成工单
        var workCountFuture = CompletableFuture.supplyAsync(() -> getCountByUserId(userId, start, end, VMSystem.TASK_STATUS_FINISH))
                .whenComplete((r, e) -> {
                    if (e != null) {
                        userWork.setWorkCount(0);
                        log.error("user work error",e);
                    } else {
                        userWork.setWorkCount(r);
                    }
                });
        //拒绝工单
        var cancelCountFuture = CompletableFuture.supplyAsync(() -> getCountByUserId(userId, start, end, VMSystem.TASK_STATUS_CANCEL))
                .whenComplete((r, e) -> {
                    if (e != null) {
                        userWork.setCancelCount(0);
                        log.error("user work error",e);
                    } else {
                        userWork.setCancelCount(r);
                    }
                });
        //进行中工单
        var progressTotalFuture = CompletableFuture.supplyAsync(() -> getCountByUserId(userId, start, end, VMSystem.TASK_STATUS_PROGRESS))
                .whenComplete((r, e) -> {
                    if (e != null) {
                        userWork.setProgressTotal(0);
                        log.error("user work error",e);
                    } else {
                        userWork.setProgressTotal(r);
                    }
                });
        CompletableFuture.allOf(
                totalFuture,
                workCountFuture,
                cancelCountFuture,
                progressTotalFuture
        ).join();
        return userWork;
    }
    /**
     * 获取排名前10的工作量
     * @param start
     * @param end
     * @return
     */
    @Override
    public List<SingletonUserWorks> getUserWorkTop10(LocalDate start, LocalDate end, Boolean isRepair, Long regionId) {
       var qw = new QueryWrapper<TaskEntity>();
        qw.select("count (user_id) as user_id")
                .lambda()
                .ge(TaskEntity::getUpdateTime,start)
                .le(TaskEntity::getUpdateTime,end);
        if (regionId>0){
            qw.lambda().eq(TaskEntity::getRegionId,regionId);
        }
        if (isRepair){
            qw.lambda().eq(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);//2 补货 int=运维 int
        }else{
            qw.lambda().ne(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }
        List<TaskEntity> taskEntityList = this.list(qw);
        var result = taskEntityList.stream().map(t -> {
            SingletonUserWorks singletonUserWorks = new SingletonUserWorks();
            singletonUserWorks.setUserName(t.getUserName());
            singletonUserWorks.setWorkCount(t.getUserId());
            return singletonUserWorks;
        }).collect(Collectors.toList());
        return  result;
    }


    private Integer getCountByUserId(Integer userId, LocalDateTime start, LocalDateTime end, Integer taskStatus) {
        var qw = new LambdaQueryWrapper<TaskEntity>();
        qw.ge(TaskEntity::getUpdateTime, start)
                .le(TaskEntity::getUpdateTime, end);
        if (userId != null) {
            qw.eq(TaskEntity::getAssignorId, userId);
        }
        if (taskStatus != null) {
            qw.eq(TaskEntity::getTaskStatus, taskStatus);
        }
        return this.count(qw);
    }


    /**
     * 统计工单数量
     *
     * @param start
     * @param end
     * @param repair     是否是运维工单
     * @param taskStatus
     * @return
     */
    private int taskCount(LocalDateTime start, LocalDateTime end, Boolean repair, Integer taskStatus) {
        var qw = new LambdaQueryWrapper<TaskEntity>();
        qw.ge(TaskEntity::getUpdateTime, start)
                .le(TaskEntity::getUpdateTime, end);

        if (taskStatus != null) {
            qw.eq(TaskEntity::getTaskStatus, taskStatus);
        }
        if (repair) {
            qw.eq(TaskEntity::getTaskStatus, taskStatus);
        } else {
            qw.ne(TaskEntity::getTaskStatus, taskStatus);
        }
        return this.count(qw);
    }
}
































