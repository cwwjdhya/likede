package com.lkd.business;

import com.lkd.annotations.ProcessType;
import com.lkd.common.VMSystem;
import com.lkd.contract.VmStatusContract;
import com.lkd.feignService.VMService;
import com.lkd.http.viewModel.TaskViewModel;
import com.lkd.service.TaskService;
import com.lkd.utils.JsonUtil;
import com.lkd.viewmodel.VendingMachineViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@ProcessType(value = "vmStatus")
@Slf4j
public class VMStatusHandler implements MsgHandler {
    @Autowired
    private TaskService taskService;

    @Autowired
    private VMService vmService;
// todo:问题是谁发这个维修的工单（测试的时候是自己发）
    @Override
    public void process(String jsonMsg) throws IOException {
        //todo : 多看看人家是如何通过监听队列式的封装协议
        VmStatusContract vmStatusContract = JsonUtil.getByJson(jsonMsg, VmStatusContract.class);
        if (vmStatusContract == null || vmStatusContract.getStatusInfo() == null || vmStatusContract.getStatusInfo().size() == 0)
            return;
        //是非正常状态就创建工单
        try {
            if (vmStatusContract.getStatusInfo().stream().anyMatch(s -> s.isStatus() == false)) {
                VendingMachineViewModel vmInfo = vmService.getVMInfo(vmStatusContract.getInnerCode());
                Integer userId = taskService.getLeastUser(vmInfo.getRegionId().intValue(), true);

                //创建工单对象
                TaskViewModel task = new TaskViewModel();
                task.setAssignorId(userId);//执行人   就要找单子少的人
                task.setDesc("自动维修工单");
                task.setInnerCode(vmStatusContract.getInnerCode());//售货机编码
                task.setProductType(VMSystem.TASK_TYPE_REPAIR);//维修工单
                task.setCreateType(0);//自动工单
                taskService.createTask(task);
            }
        } catch (Exception e) {
            log.error("创建自动维修工单失败，msg is:" + jsonMsg);
        }
    }
}


//  private String statusCode;

//  private boolean status;



















