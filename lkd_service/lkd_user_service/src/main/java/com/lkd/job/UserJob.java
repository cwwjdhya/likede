package com.lkd.job;

import com.lkd.common.VMSystem;
import com.lkd.entity.UserEntity;
import com.lkd.service.UserService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@Slf4j
public class UserJob {
    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

//    @XxlJob("testHandler")
//    public ReturnT<String> testHandler(String param) throws Exception {
//        log.info("立可得集成xxl-job");
//        return ReturnT.SUCCESS;
//    }
@XxlJob("workCountInitJobHandler")
public ReturnT<String> workCountInitJobHandler(String param) throws Exception {
    //拿到全部的用户去做插入
    List<UserEntity> userlist = userService.list();
    userlist.forEach(userEntity -> {
        if (userEntity.getRoleId().intValue()!=0){//只考虑非管理员
            //建造数据自己的每个人的工单业务的数量
            String key=VMSystem.REGION_TASK_KEY_PREF+
                    LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd"))+
                    "."+userEntity.getRegionId()+
                    "."+userEntity.getRoleCode();
            redisTemplate.opsForZSet().add(key,userEntity.getId(),0);
            redisTemplate.expire(key, Duration.ofDays(2));
        }

    });
    return ReturnT.SUCCESS;
}
}





















