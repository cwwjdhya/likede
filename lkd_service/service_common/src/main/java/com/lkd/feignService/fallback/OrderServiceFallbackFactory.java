package com.lkd.feignService.fallback;

import com.lkd.feignService.OrderService;
import com.lkd.viewmodel.RequestPay;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderServiceFallbackFactory implements FallbackFactory<OrderService>{

    @Override
    public OrderService create(Throwable throwable) {
     log.error("微服务调用失败",throwable);
        return requestPay -> null;
    }
}
