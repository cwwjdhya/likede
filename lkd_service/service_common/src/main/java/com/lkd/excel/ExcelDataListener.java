package com.lkd.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.google.common.collect.Lists;
import com.lkd.entity.AbstractEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * T 最终 E原始
 * Excel解析批量存储
 * @param <T>
 */
@Slf4j
public class ExcelDataListener<T extends AbstractEntity,E> extends AnalysisEventListener<E> {
    private  Class<T> clazz;
    private static final int BATCH_COUNT = 500;
    private Function<Collection<T> ,Boolean> function;
    //批量存储的数据
    private List<T> list = Lists.newArrayList();
    public ExcelDataListener( Function<Collection<T>, Boolean> function,Class<T> clazz) {
        this.clazz = clazz;
        this.function = function;
    }

    //读取方法
    @Override
    public void invoke(E e, AnalysisContext analysisContext) {
        try {
            T t = clazz.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(e,t);
            list.add(t);
            if (list.size()>=BATCH_COUNT){
                this.function.apply(list);
            }
        } catch (Exception ex) {
           log.error("读取excel失败");
        }
    }
    //保存方法
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
       this.function.apply(list);
    }

}
