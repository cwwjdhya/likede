package com.lkd.viewmodel;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserWork implements Serializable {
    /**
     * userId
     */
    private Integer userId;
    /**
     * 完成工单数
     */
    private Integer workCount;
    /**
     * 当日进行中的工单
     */
    private Integer progressTotal;
    /**
     * 拒绝工单数
     */
    private Integer cancelCount;
    /**
     * 工单总数
     */
    private Integer total;
}
