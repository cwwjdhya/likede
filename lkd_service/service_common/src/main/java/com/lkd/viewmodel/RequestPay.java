package com.lkd.viewmodel;

import lombok.Data;

import java.io.Serializable;

/**
 * 小程序端支付请求对象
 */
@Data
public class RequestPay implements Serializable {
    /**
     * 机器号
     */
    public  String innerCode;
    /**
     * 商品id
     */
    public  String skuId;
    /**
     * 临时串
     */
    public  String jsCode;
    /**
     * 用户唯一标识符
     */
    public  String OpenId;
}