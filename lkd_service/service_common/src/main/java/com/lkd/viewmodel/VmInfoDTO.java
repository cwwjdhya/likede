package com.lkd.viewmodel;

import lombok.Data;

import java.io.Serializable;
@Data
public class VmInfoDTO implements Serializable {
    /**
     * 经纬度坐标位置
     */
    private String location;

    /**
     * 		售货机编号
     */
    private String innerCode;
    /**
     *	点位名称
     */
    private String nodeName;
    /**
     * 详细地址
     */
    private String addr;
    /**
     * 直线距离(单位米)
     */
    private String distance;
    /**
     * 售货机类型名
     */
    private String typeName;

}
