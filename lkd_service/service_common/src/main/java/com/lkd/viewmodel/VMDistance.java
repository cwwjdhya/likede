package com.lkd.viewmodel;

import lombok.Data;

import java.io.Serializable;
//就是存一下位置信息
@Data
public class VMDistance implements Serializable {
    /**
     * 设备编号
     */
    public String innerCode;
    /**
     * 地址
     */
    public String addr;//无需传入
    /**
     * 节点名
     */
    public String nodeName;//无需传入
    /**
     * 维度
     */
    public Double lat;
    /**
     * 精度
     */
    public Double lon;

}
