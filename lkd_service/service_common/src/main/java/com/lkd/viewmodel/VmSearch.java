package com.lkd.viewmodel;

import lombok.Data;

import java.io.Serializable;
@Data
public class VmSearch  implements Serializable {
    /**
     * 	坐标点纬度
     */
    private Double lat;

    /**
     * 	坐标点维度
     */
    private Double lon;
    /**
     * 搜索半径,单位:米
     */
    private Integer distance;
}
