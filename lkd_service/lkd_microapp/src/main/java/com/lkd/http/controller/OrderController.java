package com.lkd.http.controller;

import com.lkd.common.VMSystem;
import com.lkd.config.ConsulConfig;
import com.lkd.exception.LogicException;
import com.lkd.feignService.OrderService;
import com.lkd.feignService.VMService;
import com.lkd.service.WxService;
import com.lkd.utils.DistributedLock;
import com.lkd.viewmodel.OrderViewModel;
import com.lkd.viewmodel.Pager;
import com.lkd.viewmodel.RequestPay;
import jdk.internal.joptsimple.internal.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {
    @Autowired
    private ConsulConfig consulConfig;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private WxService wxService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private VMService vmService;
    //查询订单服务

    /**
     *
     * @param pageIndex
     * @param pageSize
     * @param orderNo
     * @param openId
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping("/search")
    public Pager<OrderViewModel> search(
            @RequestParam(value = "pageIndex",required = false,defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize,
            @RequestParam(value = "orderNo",required = false,defaultValue = "") String orderNo,
            @RequestParam(value = "openId",required = false,defaultValue = "") String openId,
            @RequestParam(value = "startDate",required = false,defaultValue = "") String startDate,
            @RequestParam(value = "endDate",required = false,defaultValue = "") String endDate){
        return orderService.search(pageIndex,pageSize,orderNo,openId,startDate,endDate);
    }

    //请求支付，远程调用订单服务
    @RequestMapping("requestPay")
    public String requestPay(@RequestBody RequestPay requestPay) {
        if (!vmService.hasCapacity(requestPay.getInnerCode(),Long.valueOf(requestPay.getSkuId()))){
            throw new LogicException("该商品已售空");
        }
        //这里需要看到唯一标识符是否传过来
        if (Strings.isNullOrEmpty(requestPay.getOpenId())) {
            requestPay.setOpenId(wxService.getOpenId(requestPay.getJsCode()));
        }
        //分布式锁只能处理一个订单请求
        DistributedLock distributedLock = new DistributedLock(consulConfig.getConsulRegisterHost(), consulConfig.getConsulRegisterPort());
        DistributedLock.LockContext lock = distributedLock.getLock(requestPay.getInnerCode(), 60);
        if (!lock.isGetLock()){
            throw  new LogicException("其他设备正在处理出货请求");
        }
        redisTemplate.opsForValue().set(VMSystem.VM_LOCK_KEY_PREF+requestPay.getInnerCode(),
                lock.getSession(), Duration.ofSeconds(60));
        String responseData = orderService.requestPay(requestPay);
        if (Strings.isNullOrEmpty(responseData)) {
            throw new LogicException("微信接口调用失败");
        }
        return responseData;
    }
    //取消订单
    @RequestMapping("/cancelPay/{innerCode}/{orderNo}")
    public void cancelPay(@ PathVariable String innerCode,@ PathVariable String orderNo) {
        //解锁小程序端调用订单微服务
        DistributedLock distributedLock = new DistributedLock(consulConfig.getConsulRegisterHost(), consulConfig.getConsulRegisterPort());
        String releaseLock = redisTemplate.opsForValue().get(VMSystem.VM_LOCK_KEY_PREF +innerCode);
        if (Strings.isNullOrEmpty(releaseLock))return ;
        try {
            distributedLock.releaseLock(releaseLock);
           orderService.cancel(orderNo);
        } catch (Exception e) {
           log.error("取消订单出错");
        }

    }

    /**
     * 获取openId
     *
     * @param jsCode
     * @return
     */
    @GetMapping("/openid/{jsCode}")
    public String getOpenid(@PathVariable String jsCode) {
        return wxService.getOpenId(jsCode);
    }
}